package com.andresriofrio.ece151.distributedmutex;

import java.util.concurrent.atomic.AtomicInteger;

public class LogicalClock {
    private AtomicInteger localTimestamp = new AtomicInteger(0);

    public int tick() {
        return localTimestamp.incrementAndGet();
    }

    public int update(int remoteTimestamp) {
        return localTimestamp.updateAndGet(localTimestamp -> {
            return Math.max(localTimestamp, remoteTimestamp) + 1;
        });
    }

    public int getTimestamp() {
        return localTimestamp.get();
    }
}
