package com.andresriofrio.ece151.distributedmutex;

import java.io.IOException;
import java.io.Serializable;

public interface Duplex<T> {
    public Message<T> send(T object) throws IOException;
    public Message<T> receive() throws IOException;

    public static interface Message<T> {
        public T get();
    }

    public static class SimpleMessage<T> implements Message<T>, Serializable {
        private T object;

        public SimpleMessage(T object) {
            this.object = object;
        }

        @Override
        public T get() {
            return object;
        }

        @Override
        public String toString() {
            return "SimpleMessage{" +
                    "object=" + object +
                    '}';
        }
    }
}
