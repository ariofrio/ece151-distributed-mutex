package com.andresriofrio.ece151.distributedmutex;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.function.Consumer;

public class TotallyOrderedMulticast<P extends Comparable<P>, T> implements Multicast<P, T> {
    private final int peers;
    private final LogicalClockMulticast<P, Transmittable> clockMulticast;

    private final PriorityQueue<LogicalClockMulticast.Message<P, Transmittable>> receivedMessages = new PriorityQueue<>();
    private final HashMap<LogicalClockMulticast.Message.Timestamp<P>, Integer> receivedAcknowledgements = new HashMap<>();
    private final BlockingQueue<Message<P, T>> deliverableMessages = new LinkedBlockingDeque<>();
    private final Object receivedMessagesLock = new Object();
    private final Debugger debugger = new Debugger();

    public TotallyOrderedMulticast(int peers, LogicalClockMulticast<P, Transmittable> clockMulticast) {
        this.peers = peers;
        this.clockMulticast = clockMulticast;
        start();
    }

    private void start() {
        Threads.startPropagated(() -> {
            while (true) {
                LogicalClockMulticast.Message<P, Transmittable> clockMessage = clockMulticast.receive();
                if (clockMessage.get() instanceof Message) {
                    receiveMessage(clockMessage);
                } else if (clockMessage.get() instanceof Acknowledgement) {
                    Acknowledgement<P> acknowledgement = (Acknowledgement<P>) clockMessage.get();
                    LogicalClockMulticast.Message.Timestamp<P> key = acknowledgement.getReceivedMessageTimestamp();
                    receivedAcknowledgements.compute(key, (k, v) -> (v == null) ? 1 : v + 1);
                }
                deliverAnyMessages();
            }
        });
    }

    private void receiveMessage(LogicalClockMulticast.Message<P, Transmittable> clockMessage) throws IOException {
        synchronized (receivedMessagesLock) {
            receivedMessages.add(clockMessage);
        }
        clockMulticast.send(new Acknowledgement<>(clockMessage.getTimestamp()));
    }

    private void deliverAnyMessages() {
        while (true) {
            synchronized (receivedMessagesLock) {
                LogicalClockMulticast.Message<P, Transmittable> topMessage = receivedMessages.peek();
                if (topMessage == null) return;

                int count = receivedAcknowledgements.getOrDefault(topMessage.getTimestamp(), 0);
                if (count < peers) {
                    return;
                } else if (count > peers) {
                    throw new AssertionError("acknowledgement count for message " + topMessage.getTimestamp() + " is " + count + " but there are only " + peers + " peers");
                } else {
                    // Insert the message into deliverableMessage only if it has space. Otherwise, wait until next round.
                    Message<P, T> message = (Message<P, T>) topMessage.get();
                    message = new Message<>(topMessage.getTimestamp(), message.get());
                    final boolean didInsert = deliverableMessages.offer(message);
                    if (!didInsert) return; else receivedMessages.remove();
                }
            }
        }
    }

    @Override
    public Message<P, T> send(T object) throws IOException {
        Message<P, T> message = new Message<>(object);
        debugger.beforeSend.accept(message);
        LogicalClockMulticast.Message<P, Transmittable> clockMessage = clockMulticast.send(message);
        message.setTimestamp(clockMessage.getTimestamp());
        receiveMessage(clockMessage); // conceptually send to myself
        return message;
    }

    @Override
    public Message<P, T> receive() throws IOException {
        Message<P, T> message = Throwables.propagate(deliverableMessages::take, IOException::new);
        debugger.beforeDeliver.accept(message);
        return message;
    }

    public static class Message<P extends Comparable<P>, T> implements Multicast.Message<P, T>, Transmittable {
        private @Nullable LogicalClockMulticast.Message.Timestamp<P> timestamp;
        private final T object;

        public Message(@Nullable LogicalClockMulticast.Message.Timestamp<P> timestamp, T object) {
            this.timestamp = timestamp;
            this.object = object;
        }

        public Message(T object) {
            this(null, object);
        }

        @Nullable
        @Override
        public P getPeer() {
            if (timestamp != null) {
                return timestamp.getLowOrder();
            } else {
                return null;
            }
        }

        @Override
        public T get() {
            return object;
        }

        @Nullable
        public LogicalClockMulticast.Message.Timestamp<P> getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(@Nullable LogicalClockMulticast.Message.Timestamp<P> timestamp) {
            this.timestamp = timestamp;
        }

        @Override
        public String toString() {
            return "Message{" +
                    "timestamp=" + timestamp +
                    ", object=" + object +
                    '}';
        }

    }

    public static class Acknowledgement<P extends Comparable<P>> implements Transmittable {
        private final LogicalClockMulticast.Message.Timestamp<P> receivedMessageTimestamp;

        public Acknowledgement(LogicalClockMulticast.Message.Timestamp<P> receivedMessageTimestamp) {
            this.receivedMessageTimestamp = receivedMessageTimestamp;
        }

        public LogicalClockMulticast.Message.Timestamp<P> getReceivedMessageTimestamp() {
            return receivedMessageTimestamp;
        }

        @Override
        public String toString() {
            return "Acknowledgement{" + receivedMessageTimestamp + '}';
        }
    }

    public static interface Transmittable extends Serializable {}

    public Debugger getDebugger() {
        return debugger;
    }

    public class Debugger {
        private Consumer<Message<P, T>> beforeSend = m -> {};
        public Consumer<Message<P, T>> beforeDeliver = m -> {};

        public void onBeforeSend(Consumer<Message<P, T>> handler) {
            beforeSend = beforeSend.andThen(handler);
        }

        public void onBeforeDeliver(Consumer<Message<P, T>> handler) {
            beforeDeliver = beforeDeliver.andThen(handler);
        }
    }

}
