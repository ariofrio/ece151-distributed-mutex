package com.andresriofrio.ece151.distributedmutex;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.function.Consumer;
import java.util.function.Function;

public class LogicalClockMulticast<P extends Comparable<P>, T> implements Multicast<P, T> {
    private final LogicalClock clock;
    private final Iterable<? extends Duplex<Message<P, T>>> peers;
    private final P localLabel;
    private final BlockingQueue<Message<P, T>> deliverableMessages = new LinkedBlockingDeque<>();
    private final Debugger debugger = new Debugger();

    public LogicalClockMulticast(LogicalClock clock, Iterable<? extends Duplex<Message<P, T>>> peers, P localLabel) {
        this.clock = clock;
        this.peers = peers;
        this.localLabel = localLabel;
        start();
    }

    private void start() {
        for (Duplex<Message<P, T>> peer : peers) {
            Threads.startPropagated(() -> {
                while (true) {
                    Message<P, T> message = peer.receive().get();
                    clock.update(message.getTimestamp().getHighOrder());
                    debugger.afterReceive.accept(message);
                    deliverableMessages.put(message);
                }
            });
        }
    }

    @Override
    public Message<P, T> send(T object) throws IOException {
        Message<P, T> message = new Message<>(new Message.Timestamp<P>(clock.tick(), localLabel), object);
        debugger.beforeSend.accept(message);
        for (Duplex<Message<P, T>> peer : peers) {
            peer.send(message);
        }
        return message;
    }

    @Override
    public Message<P, T> receive() throws IOException {
        Message<P, T> message = Throwables.propagate(deliverableMessages::take);
        debugger.beforeDeliver.accept(message);
        return message;
    }

    public static class Message<P extends Comparable<P>, T> implements Multicast.Message<P, T>, Comparable<Message<P, T>>, Serializable {
        private final @NotNull Timestamp<P> timestamp;
        private final T object;

        public Message(@NotNull Timestamp<P> timestamp, T object) {
            this.timestamp = timestamp;
            this.object = object;
        }

        @NotNull
        @Override
        public P getPeer() {
            return timestamp.getLowOrder();
        }

        @Override
        public T get() {
            return object;
        }

        @NotNull
        public Timestamp<P> getTimestamp() {
            return timestamp;
        }

        @Override
        public String toString() {
            return "Message{" +
                    "timestamp=" + timestamp +
                    ", object=" + object +
                    '}';
        }

        @Override
        public int compareTo(@NotNull Message<P, T> o) {
            return timestamp.compareTo(o.timestamp);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Message message = (Message) o;

            if (!timestamp.equals(message.timestamp)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return timestamp.hashCode();
        }

        public static class Timestamp<P extends Comparable<P>> implements Comparable<Timestamp<P>>, Serializable {
            private final int highOrder;
            private final @NotNull P lowOrder;

            public Timestamp(int highOrder, @NotNull P lowOrder) {
                this.highOrder = highOrder;
                this.lowOrder = lowOrder;
            }

            @Override
            public int compareTo(@NotNull Timestamp<P> o) {
                int higherOrderComparison = Integer.compare(highOrder, o.highOrder);
                if (higherOrderComparison == 0) {
                    return lowOrder.compareTo(o.lowOrder);
                } else {
                    return higherOrderComparison;
                }
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                Timestamp timestamp = (Timestamp) o;

                if (highOrder != timestamp.highOrder) return false;
                if (!lowOrder.equals(timestamp.lowOrder)) return false;

                return true;
            }

            @Override
            public int hashCode() {
                int result = highOrder;
                result = 31 * result + (lowOrder.hashCode());
                return result;
            }

            public int getHighOrder() {
                return highOrder;
            }

            @NotNull
            public P getLowOrder() {
                return lowOrder;
            }

            @Override
            public String toString() {
                return highOrder + "." + lowOrder;
            }
        }
    }

    public Debugger getDebugger() {
        return debugger;
    }

    public class Debugger {
        private Consumer<Message<P, T>> beforeSend = m -> {};
        public Consumer<Message<P, T>> beforeDeliver = m -> {};
        public Consumer<Message<P, T>> afterReceive = m -> {};

        public void onBeforeSend(Consumer<Message<P, T>> handler) {
            beforeSend = beforeSend.andThen(handler);
        }

        public void onBeforeDeliver(Consumer<Message<P, T>> handler) {
            beforeDeliver = beforeDeliver.andThen(handler);
        }

        public void onAfterReceive(Consumer<Message<P, T>> handler) {
            afterReceive = afterReceive.andThen(handler);
        }
    }

}
