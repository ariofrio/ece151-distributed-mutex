package com.andresriofrio.ece151.distributedmutex;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws IOException {
        int[] ports = IntStream.range(0, 3).map(i -> 8000 + i).toArray();
        CyclicBarrier connectionBarrier = new CyclicBarrier(ports.length);

        final AtomicBoolean test = new AtomicBoolean(false);

        for (int localPort : ports) {
            Threads.startPropagated(() -> {
                LogicalClock clock = new LogicalClock();
                Consumer<Object> log = message -> System.out.println(clock.getTimestamp() + "." + localPort + ": " + message);

                // Set up connections
                ServerSocket serverSocket = new ServerSocket(localPort);
                connectionBarrier.await();
                List<Socket> sockets = new ArrayList<>();
                for (int remotePort : ports) {
                    if (remotePort != localPort) {
                        sockets.add(new Socket("127.0.0.1", remotePort));
                    }
                }
//                Multicast<String, String> multicast = setUpTotallyOrderedMulticast(ports, localPort, clock, serverSocket, sockets);
                DistributedMutex<String> mutex = setUpDistributedMutex(ports, localPort, clock, serverSocket, sockets, log);

                // Start the clock
                Threads.startPropagated(() -> {
                    while (true) {
                        clock.tick();
                        log.accept("");
                        Thread.sleep(500 * (localPort - 8000 + 1));
                    }
                });

//                // Send a message randomly
//                Threads.startPropagated(() -> {
//                    while (true) {
//                        Thread.sleep((long) (Math.random() * 0.1 * 1000));
////                        String message = "Hi world from " + localPort + " #" + (long) (Math.random() * 100000);
//                        multicast.send(message);
//                    }
//                });
//
//                // Receive messages
//                Threads.startPropagated(() -> {
//                    while (true) {
//                        log.accept("received " + multicast.receive());
//                    }
//                });

                // Acquire randomly
                Threads.startPropagated(() -> {
                    while (true) {
                        Thread.sleep((long) (Math.random() * 10 * 1000));
                        mutex.lock();
                        log.accept("Acquired!");
                        if(test.get()) throw new IllegalStateException("oh noes, we have a conflict!");
                        test.set(true);
                        Thread.sleep(1000);
                        test.set(false);
                        mutex.unlock();
                        log.accept("Released!");
                    }
                });

            });
        }
    }

    private static DistributedMutex<String> setUpDistributedMutex(int[] ports, int localPort, LogicalClock clock, ServerSocket serverSocket, List<Socket> sockets, Consumer<Object> log) throws IOException {
        // Set up basic connections
        List<ObjectStreamDuplex<LogicalClockMulticast.Message<String, TotallyOrderedMulticast.Transmittable>>>
                peers = ObjectStreamDuplex.findPeersFromSockets(serverSocket, sockets, String.valueOf(localPort));

        // Set up middleware
        LogicalClockMulticast<String, TotallyOrderedMulticast.Transmittable> clockMulticast = new LogicalClockMulticast<>(clock, peers, String.valueOf(localPort));
        TotallyOrderedMulticast<String, DistributedMutex.Transmittable> orderedMulticast = new TotallyOrderedMulticast<>(ports.length - 1, clockMulticast);
        DistributedMutex<String> distributedMutex = new DistributedMutex<>(ports.length - 1, orderedMulticast);

        // Set up debug hooks
//        clockMulticast.getDebugger().onBeforeSend(message -> log.accept("LogicalClockMulticast broadcasting " + message));
//        clockMulticast.getDebugger().onBeforeDeliver(message -> log.accept("LogicalClockMulticast received " + message));
//        orderedMulticast.getDebugger().onBeforeSend(message -> log.accept("Broadcasting " + message));
//        orderedMulticast.getDebugger().onBeforeDeliver(message -> log.accept("Received " + message));
        distributedMutex.getDebugger().onSendRequest(() -> log.accept("Broadcasting request..."));
        distributedMutex.getDebugger().onReceiveOK(ts -> log.accept("Received OK from " + ts.getLowOrder()));
        distributedMutex.getDebugger().onReceiveRequest(ts -> log.accept("Received request from " + ts.getLowOrder() + " at time " + ts));

        return distributedMutex;
    }

    private static Multicast<String, String> setUpTotallyOrderedMulticast(int[] ports, int localPort, LogicalClock clock, ServerSocket serverSocket, List<Socket> sockets) throws IOException {
        // Set up basic connections
        List<ObjectStreamDuplex<LogicalClockMulticast.Message<String, TotallyOrderedMulticast.Transmittable>>>
                peers = ObjectStreamDuplex.findPeersFromSockets(serverSocket, sockets, String.valueOf(localPort));

        // Set up middleware
        LogicalClockMulticast<String, TotallyOrderedMulticast.Transmittable> clockMulticast = new LogicalClockMulticast<>(clock, peers, String.valueOf(localPort));
        TotallyOrderedMulticast<String, String> orderedMulticast = new TotallyOrderedMulticast<>(ports.length - 1, clockMulticast);

        // Set up debug hooks
//        clockMulticast.getDebugger().onBeforeSend(message -> log.accept("LogicalClockMulticast broadcasting " + message));
//        clockMulticast.getDebugger().onBeforeDeliver(message -> log.accept("LogicalClockMulticast received " + message));
        return orderedMulticast;
    }

    private static Multicast<String, String> setUpLogicalClockMulticast(int[] ports, int localPort, LogicalClock clock, ServerSocket serverSocket, List<Socket> sockets) throws IOException {
        // Set up basic connections
        List<ObjectStreamDuplex<LogicalClockMulticast.Message<String, String>>> peers = ObjectStreamDuplex.findPeersFromSockets(serverSocket, sockets, String.valueOf(localPort));

        // Set up middleware
        return new LogicalClockMulticast<>(clock, peers, String.valueOf(localPort));
    }



}
