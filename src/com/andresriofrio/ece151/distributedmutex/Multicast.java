package com.andresriofrio.ece151.distributedmutex;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.Serializable;

public interface Multicast<P, T> extends Duplex<T>  {
    @Override
    Message<P, T> send(T object) throws IOException;

    @Override
    Message<P, T> receive() throws IOException;

    public static interface Message<P, T> extends Duplex.Message<T> {
        public @Nullable P getPeer();
        public T get();
    }

    public static class SimpleMessage<P, T> implements Message<P, T>, Serializable {
        private T object;
        private @Nullable P peer;

        public SimpleMessage(T object) {
            this(null, object);
        }

        public SimpleMessage(@Nullable P peer, T object) {
            this.object = object;
            this.peer = peer;
        }

        @Nullable
        @Override
        public P getPeer() {
            return peer;
        }

        public void setPeer(@Nullable P peer) {
            this.peer = peer;
        }

        @Override
        public T get() {
            return object;
        }

        @Override
        public String toString() {
            return "SimpleMessage{" +
                    "object=" + object +
                    ", peer=" + peer +
                    '}';
        }
    }
}
