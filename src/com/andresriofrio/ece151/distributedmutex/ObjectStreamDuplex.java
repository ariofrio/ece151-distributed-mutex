package com.andresriofrio.ece151.distributedmutex;

import sun.misc.OSEnvironment;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class ObjectStreamDuplex<T> implements Duplex<T> {
    private final ObjectInputStream inputStream;
    private final ObjectOutputStream outputStream;
    private final String remoteLabel;
    private final Object sendLock = new Object();
    private final Object receiveLock = new Object();

    public ObjectStreamDuplex(ObjectInputStream inputStream, ObjectOutputStream outputStream, String remoteLabel) throws IOException {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        this.remoteLabel = remoteLabel;
    }

    public ObjectStreamDuplex(InputStream rawInputStream, OutputStream rawOutputStream, String remoteLabel) throws IOException {
        this(new ObjectInputStream(rawInputStream), new ObjectOutputStream(rawOutputStream), remoteLabel);
    }

    public ObjectStreamDuplex(Socket inputSocket, Socket outputSocket, String remoteLabel) throws IOException {
        this(inputSocket.getInputStream(), outputSocket.getOutputStream(), remoteLabel);
    }

    @Override
    public SimpleMessage<T> send(T object) throws IOException {
        SimpleMessage<T> message = new SimpleMessage<>(object);
        synchronized (sendLock) {
            outputStream.writeObject(message);
        }
        return message;
    }

    @Override
    public SimpleMessage<T> receive() throws IOException {
        try {
            synchronized (receiveLock) {
                return (SimpleMessage<T>) inputStream.readObject();
            }
        } catch (ClassNotFoundException e) {
            throw new IOException(e);
        }
    }

    @Override
    public String toString() {
        if (remoteLabel != null) {
            return remoteLabel;
        } else {
            return super.toString();
        }
    }

    public static <T> List<ObjectStreamDuplex<T>> findPeersFromSockets(
            ServerSocket serverSocket, Collection<Socket> outgoingSockets, String localId) throws IOException {
        Map<String, ObjectInputStream> incomingStreamsById = new HashMap<>();
        Map<String, ObjectOutputStream> outgoingStreamsById = new HashMap<>();
        CyclicBarrier doneBarrier = new CyclicBarrier(outgoingSockets.size() + 1);

        for (Socket outgoingSocket : outgoingSockets) {
            Threads.startPropagated(() -> {
                // It's important to create the ObjectOutputStream that will be used by the ObjectStreamDuplex
                // in the background like so because both Object{Output,Input}Stream wait for the other to respond
                // with the appropriate headers.
                ObjectOutputStream outgoingStream = new ObjectOutputStream(outgoingSocket.getOutputStream());
                outgoingStream.writeObject(localId);
                String remoteId = Throwables.propagate(() ->
                        (String) new ObjectInputStream(outgoingSocket.getInputStream()).readObject());
                outgoingStreamsById.put(remoteId, outgoingStream);
                doneBarrier.await();
            });
        }

        for (int i = 0; i < outgoingSockets.size(); i++) {
            Socket incomingSocket = serverSocket.accept();
            ObjectInputStream incomingStream = new ObjectInputStream(incomingSocket.getInputStream());
            String remoteId = Throwables.propagate(() -> (String) incomingStream.readObject());
            new ObjectOutputStream(incomingSocket.getOutputStream()).writeObject(localId);
            incomingStreamsById.put(remoteId, incomingStream);
        }
        Throwables.propagate(doneBarrier::await);

        Set<String> remoteIds = incomingStreamsById.keySet();
        return remoteIds.stream().map(remoteId -> {
            try {
                return new ObjectStreamDuplex<T>(incomingStreamsById.get(remoteId), outgoingStreamsById.get(remoteId), remoteId);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }


}
