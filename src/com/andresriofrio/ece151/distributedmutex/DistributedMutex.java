package com.andresriofrio.ece151.distributedmutex;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

public class DistributedMutex<P extends Comparable<P>> {
    private final int peers;
    private final TotallyOrderedMulticast<P, Transmittable> orderedMulticast;
    private final Debugger debugger = new Debugger();

    private ConcurrentLinkedQueue<TotallyOrderedMulticast.Message<P, Transmittable>> requestQueue = new ConcurrentLinkedQueue<>();
    private State state = new LockUnwantedState();

    public DistributedMutex(int peers, TotallyOrderedMulticast<P, Transmittable> orderedMulticast) {
        this.peers = peers;
        this.orderedMulticast = orderedMulticast;
        start();
    }

    private void start() {
        Threads.startPropagated(() -> {
            while (true) {
                TotallyOrderedMulticast.Message<P, Transmittable> receivedMessage = orderedMulticast.receive();
                synchronized (DistributedMutex.this) {
                    if (receivedMessage.get() instanceof Request) {
                        if (state instanceof LockUnwantedState) {
                            debugger.onReceiveRequest.accept(receivedMessage.getTimestamp());
                            grantPermission(receivedMessage);
                        } else if (state instanceof LockOwnedState) {
                            debugger.onReceiveRequest.accept(receivedMessage.getTimestamp());
                            requestQueue.add(receivedMessage);
                        } else if (state instanceof LockWantedState) {
                            TotallyOrderedMulticast.Message sentMessage = ((LockWantedState) state).sentMessage;
                            int compare = receivedMessage.getTimestamp().compareTo(sentMessage.getTimestamp()); // HERE TODO DODODO
                            if (compare < 0) { // received message timestamp is lower than our sent message
                                debugger.onReceiveRequest.accept(receivedMessage.getTimestamp());
                                grantPermission(receivedMessage);
                            } else if (compare > 0) { // our sent message timestamp is lower than received message
                                debugger.onReceiveRequest.accept(receivedMessage.getTimestamp());
                                requestQueue.add(receivedMessage);
                            } else {
                                // this _is_ the message we sent (TotallyOrderedMulticast sends it back to us)
                                // ignore
                            }
                        } else {
                            throw new IllegalStateException();
                        }
                    } else if (receivedMessage.get() instanceof OK) {
                        if (state instanceof LockWantedState) {
                            LockWantedState wantedState = (LockWantedState) state;
                            OK<P> ok = (OK<P>) receivedMessage.get();
                            if (ok.destination.equals(wantedState.sentMessage.getPeer())) {
                                debugger.onReceiveOK.accept(receivedMessage.getTimestamp());
                                wantedState.permissionLatch.countDown();
                            }
                        }
                    }
                }
            }
        });
    }

    private void grantPermission(TotallyOrderedMulticast.Message<P, Transmittable> receivedMessage) throws IOException {
        orderedMulticast.send(new OK(receivedMessage.getPeer()));
    }

    public void lock() throws IOException, InterruptedException {
        if (state instanceof LockUnwantedState) {
            LockWantedState wantedState;
            synchronized (this) {
                debugger.onSendRequest.run();
                wantedState = transition(new LockWantedState<>(
                        new CountDownLatch(peers),
                        orderedMulticast.send(new Request())));
            }
            wantedState.permissionLatch.await();
            transition(new LockOwnedState());
        } else {
            throw new IllegalStateException();
        }
    }

    private <T extends State> T transition(T state) {
        this.state = state;
        return state;
    }

    public synchronized void unlock() throws IOException {
        if (state instanceof LockOwnedState) {
            // send OK messages to all processes in my queue and remove them from my queue
            TotallyOrderedMulticast.Message<P, Transmittable> request;
            while((request = requestQueue.poll()) != null) {
                grantPermission(request);
            }

            transition(new LockUnwantedState());
        } else {
            throw new IllegalStateException();
        }
    }

    private static class LockOwnedState implements State {}

    private static class LockWantedState<P extends Comparable<P>> implements State {
        final CountDownLatch permissionLatch;
        final TotallyOrderedMulticast.Message<P, Transmittable> sentMessage;

        LockWantedState(CountDownLatch permissionLatch, TotallyOrderedMulticast.Message<P, Transmittable> sentMessage) throws IOException {
            this.permissionLatch = permissionLatch;
            this.sentMessage = sentMessage;
        }
    }

    private static class LockUnwantedState implements State {}

    private interface State {}

    private static class OK<P> implements Transmittable {
        private final P destination;

        private OK(P destination) {
            this.destination = destination;
        }

        public P getDestination() {
            return destination;
        }

        @Override
        public String toString() {
            return "OK{" +
                    "destination=" + destination +
                    '}';
        }
    }
    private static class Request implements Transmittable {
        @Override
        public String toString() {
            return "Request{}";
        }
    }
    public static interface Transmittable extends Serializable {}

    public Debugger getDebugger() {
        return debugger;
    }

    public class Debugger {
        public Consumer<LogicalClockMulticast.Message.Timestamp<P>> onReceiveRequest = m -> {};
        public Consumer<LogicalClockMulticast.Message.Timestamp<P>> onReceiveOK = m -> {};
        public Runnable onSendRequest = () -> {};

        public void onReceiveRequest(Consumer<LogicalClockMulticast.Message.Timestamp<P>> handler) {
            onReceiveRequest = onReceiveRequest.andThen(handler);
        }

        public void onReceiveOK(Consumer<LogicalClockMulticast.Message.Timestamp<P>> handler) {
            onReceiveOK = onReceiveOK.andThen(handler);
        }

        public void onSendRequest(Runnable handler) {
            Runnable oldHandler = onSendRequest;
            onSendRequest = () -> {
                oldHandler.run();
                handler.run();
            };
        }
    }

}
